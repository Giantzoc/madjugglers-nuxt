const pkg = require('./package')

module.exports = {
  ssr: true,
  target: 'static',
  static: {
    prefix: false,
  },
  generate: {
    dir: 'dist',
    fallback: true,
  },

  env: {
    functions:
      process.env.NODE_ENV === 'production'
        ? `https://madjugglers.com/.netlify/functions`
        : 'http://localhost:9000/.netlify/functions',
  },

  /*
   ** Headers of the page
   */
  head: {
    title: pkg.name,
    noscript: [
      {
        innerHTML: 'Sorry, this website requires JavaScript to work correctly.',
      },
    ],
    htmlAttrs: {
      lang: 'en',
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
  },

  manifest: {
    name: 'Madison Area Jugglers',
    short_name: 'MAJ',
    lang: 'en',
  },
  icon: {
    iconSrc: 'static/MAJ_Favicon.png',
  },

  /*
   ** Customize the progress-bar color
   */
  loading: { color: '#fff' },

  /*
   ** Global CSS
   */
  css: [
    '@/assets/css/bootstrap.css',
    //    "@/assets/css/bootswatch.scss"
  ],

  /*
   ** Plugins to load before mounting the App
   */
  plugins: [],

  /*
   ** Nuxt.js modules
   */
  modules: [
    'nuxt-leaflet',
    // Doc: https://bootstrap-vue.js.org/docs/
    'bootstrap-vue/nuxt',
    '@nuxtjs/pwa',
    'nuxt-responsive-loader',
    'nuxt-material-design-icons',
  ],
  router: {
    extendRoutes(routes, resolve) {
      routes.push({
        name: 'madfest2019',
        path: '/madfest2019',
        component: resolve(__dirname, 'pages/madfest.vue'),
      }),
        routes.push({
          name: 'madfest2018',
          path: '/madfest2018',
          component: resolve(__dirname, 'pages/madfest.vue'),
        }),
        routes.push({
          name: 'madfest2020',
          path: '/madfest2020',
          component: resolve(__dirname, 'pages/madfest.vue'),
        })
    },
  },
  /*
   ** nuxt-responsive-loader settings
   */
  responsiveLoader: {
    name: 'img/[hash:7]-[width].[ext]',
    min: 200, // minimum image width generated
    max: 1080, // maximum image width generated
    steps: 5, // five sizes per image will be generated
    placeholder: true, // no placeholder will be generated
    quality: 90, // images are compressed with medium quality
  },
  /*
   ** Build configuration
   */
  build: {
    /*
    ** You can extend webpack config here

    config.output.publicPath = './_nuxt/'
    */
    extend(config, ctx) {
      // Run ESLint on save
      // if (ctx.isDev && ctx.isClient) {
      //   config.module.rules.push({
      //     enforce: 'pre',
      //     test: /\.(js|vue)$/,
      //     //loader: 'eslint-loader',
      //     exclude: /(node_modules)/
      //   })
      // }
    },
  },
}
