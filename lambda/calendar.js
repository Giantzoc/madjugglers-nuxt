const { google } = require('googleapis')
require('dotenv').config()
const apiKey = process.env.GOOGLE_APIKEY

const headers = {
  'Access-Control-Allow-Origin': '*', //TODO: change this for production?
  'Access-Control-Allow-Methods': 'POST',
  'Access-Control-Allow-Headers': 'Content-Type',
  'content-type': 'application/json'
}

var cachedResponse
var lastResponseDate

exports.handler = function(event, context, callback) {
  // only allow GET requests
  if (event.httpMethod !== 'GET') {
    return callback(null, {
      statusCode: 410,
      body: JSON.stringify({
        message: 'Only GET requests allowed.'
      })
    })
  }
  
  //check the cache first
  var currDate = new Date()
  if (
    cachedResponse != null &&
    lastResponseDate.getDate() == currDate.getDate()
  ) {
    //return cached response
    return callback(null, {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        message: 'Cached response',
        events: cachedResponse
      })
    })
  }

  // Each API may support multiple version. With this sample, we're getting
  // v3 of the calendar API, and using an API key to authenticate.
  const calendar = google.calendar({
    version: 'v3',
    auth: apiKey
  })

  var oldDate = new Date()
  oldDate.setDate(oldDate.getDate() - 1)

  const params = {
    calendarId: 'ak999c6t3dheerp3rd62q9tc98@group.calendar.google.com',
    timeMin: oldDate.toISOString(), //only collect future dates
    maxResults: 20,
    singleEvents: true,
    orderBy: 'startTime'
  }

  calendar.events.list(params, (err, res) => {
    if (err) {
      console.error(err)
      return callback(null, {
        statusCode: 500,
        body: JSON.stringify({
          message: 'Internal Server Error: ' + err
        })
      })
    }

    var events = []
    res.data.items.forEach(item => {

      var event = { title: item.summary, date: item.start.dateTime, desc: item.location }
      events.push(event)
    })

    cachedResponse = events
    lastResponseDate = currDate
    console.log(events)
    return callback(null, {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        message: 'Message sent successfully!',
        events: events
      })
    })
  })
}