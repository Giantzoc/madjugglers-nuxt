//http://localhost:9000/blog
const { google } = require('googleapis')
require('dotenv').config()
const apiKey = process.env.GOOGLE_APIKEY

const headers = {
  'Access-Control-Allow-Origin': '*', //TODO: change this for production?
  'Access-Control-Allow-Methods': 'POST',
  'Access-Control-Allow-Headers': 'Content-Type',
  'content-type': 'application/json'
}

var cachedResponse
var lastResponseDate

exports.handler = function(event, context, callback) {
  // only allow GET requests
  if (event.httpMethod !== 'GET') {
    return callback(null, {
      statusCode: 410,
      body: JSON.stringify({
        message: 'Only GET requests allowed.'
      })
    })
  }

  //check the cache first
  var currDate = new Date()
  if (
    cachedResponse != null &&
    lastResponseDate.getDate() == currDate.getDate()
  ) {
    //return cached response
    return callback(null, {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        message: 'Message sent successfully!',
        posts: cachedResponse
      })
    })
  }

  // Each API may support multiple version. With this sample, we're getting
  // v3 of the blogger API, and using an API key to authenticate.
  const blogger = google.blogger({
    version: 'v3',
    auth: apiKey
  })

  const params = {
    blogId: '8635888519111260662',
    fetchBodies: 'true',
    orderBy: 'published',
    status: 'live',
    view: 'READER',
    maxResults: '5'
  }

  blogger.posts.list(params, function(err, res) {
    if (err) {
      console.error(err)
      return callback(null, {
        statusCode: 500,
        body: JSON.stringify({
          message: 'Internal Server Error: ' + err
        })
      })
    }
    //console.log(json);
    cachedResponse = res.data.items
    lastResponseDate = currDate
    return callback(null, {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        message: 'Message sent successfully!',
        posts: res.data.items
      })
    })
  })
}
