const SparkPost = require('sparkpost')
require('dotenv').config()
const apiKey = process.env.SPARKPOST_APIKEY
console.log(apiKey)
const client = new SparkPost(apiKey)

const headers = {
  'Access-Control-Allow-Origin': '*', //TODO: change this for production?
  'Access-Control-Allow-Methods': 'POST',
  'Access-Control-Allow-Headers': 'Content-Type',
  'content-type': 'application/json'
}

exports.handler = function(event, context, callback) {
  // only allow POST requests
  if (event.httpMethod !== 'POST') {
    return callback(null, {
      statusCode: 410,
      body: JSON.stringify({
        message: 'Only POST requests allowed.'
      })
    })
  }

  // parse the body to JSON so we can use it in JS
  const payload = JSON.parse(event.body)

  // validate the form
  if (!payload.subject || !payload.email || !payload.message) {
    return callback(null, {
      statusCode: 422,
      headers,
      body: JSON.stringify({
        message: 'Required information is missing.'
      })
    })
  }

  //check the honeypot
  if (payload.hemail != '') {
    return callback(null, {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        message: 'Message sent successfully!'
      })
    })
  }

  client.transmissions
    .send({
      content: {
        from: 'webmaster@mail.madjugglers.com',
        subject: 'MAJ Contact: ' + payload.subject,
        html:
          'Reply: <a href=”mailto:' +
          payload.email +
          '”>' +
          payload.email +
          '</a></br>Message: ' +
          payload.message
      },
      recipients: [
        { address: 'steven@giantzoc.com' }
      ]
    })
    .then(data => {
      console.log(data)
      return callback(null, {
        statusCode: 200,
        headers,
        body: JSON.stringify({
          message: 'Message sent successfully!'
        })
      })
    })
    .catch(err => {
      console.error(err)
      return callback(null, {
        statusCode: 500,
        body: JSON.stringify({
          message: 'Internal Server Error: ' + err
        })
      })
    })
}
