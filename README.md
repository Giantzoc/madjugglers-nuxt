# Madison Area Jugglers Home

GitLab Source: https://giantzoc.gitlab.io/madjugglers-nuxt/

Production Site: https://madjugglers.com

## Build Setup

This site uses nuxt.js to generate static html and javascript files.  The site is automatically built and deployed to the public folder by the host (netlify) when changes are pushed.

The contact page uses netlify lamda functions.  It runs on localhost:9000 by default in dev.  Submitting the form causes an email to be automatically sent to the specified addresses.  The form has an extremely basic honeypot to try to reduce spam.  https://medium.com/@marcmintel/how-to-submit-forms-on-a-static-website-with-vuejs-and-netlify-functions-b901f40f0627

To setup your project clone the repo and install the dependencies.
``` bash
$ npm install
```

To generate the files and test the site run:

``` bash
$ npm run generate
$ npm run dev
```

To run the lambda functions for dev:
``` bash
$ npm run functions:serve
```


To test production deploy:

``` bash
npm run generate
npm run functions:build
```

The site gets automatically built and must be deployed from netlify.

For detailed explanation on how things work, checkout [Nuxt.js docs](https://nuxtjs.org).

The site is also using bootstrap-vue for the layout.  https://bootstrap-vue.js.org